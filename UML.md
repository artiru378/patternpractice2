```plantuml
@startuml

interface ICommand {
    + Execute()
}

class Invoker {
    - commands: Queue<ICommand>
    + AddCommand(command: ICommand)
    + ExecuteCommands()
}

class CastSpellCommand {
    - elf: Elf
    - spell: string
    + Execute()
}

class MineOreCommand {
    - dwarf: Dwarf
    - oreType: string
    + Execute()
}

ICommand <|.. CastSpellCommand
ICommand <|.. MineOreCommand
Invoker o-- ICommand

@enduml
```

```plantuml
@startuml
!define ICONURL https://raw.githubusercontent.com/rrobbes/EngineeringSDLC/master/icons

skinparam class {
    BackgroundColor<<interface>> #87CEEB
    BackgroundColor<<abstract>> #F0E68C
    BackgroundColor<<concrete>> #FFB6C1
}

skinparam stereotypeCBackgroundColor #ADD8E6

' Interfaces
interface "ICharacterBuilder" <<interface>> {
+SetAppearance(appearance: string): T
+SetAbilities(abilities: string): T
+SetEquipment(equipment: string): T
+Build(): void
}

' Abstract classes
abstract class "CharacterBuilder" <<abstract>> {
#character: Character
+SetAppearance(appearance: string): T
+SetAbilities(abilities: string): T
+SetEquipment(equipment: string): T
{abstract} +Build(): void
}

abstract class "Character" <<abstract>> {
+Appearance: string { get; set; }
+Abilities: string { get; set; }
+Equipment: string { get; set; }
+Race: string { get; set; }
+Level: int { get; set; }
}

' Concrete classes
class "ElfBuilder" <<concrete>> {
-ElfBuilder()
+Build(): void
+Get(): Elf
}

class "DwarfBuilder" <<concrete>> {
-DwarfBuilder()
+Build(): void
+Get(): Dwarf
}

class "Elf" <<concrete>> {
+MagicPower: int { get; set; }
+CastSpell(spell: string): void
}

class "Dwarf" <<concrete>> {
+MiningSkill: int { get; set; }
+MineOre(oreType: string): void
}

ICharacterBuilder <|-- CharacterBuilder : Inheritance

CharacterBuilder <|-- ElfBuilder : Inheritance
CharacterBuilder <|-- DwarfBuilder : Inheritance

Character <|-- Elf : Inheritance
Character <|-- Dwarf : Inheritance

ElfBuilder *-- Elf : Composition 
DwarfBuilder *-- Dwarf : Composition 

@enduml
```

```plantuml
@startuml

!define ICONURL https://raw.githubusercontent.com/rrobbes/EngineeringDrawing/master/icons/

skinparam class {
    BackgroundColor<<interface>> #87CEEB
    BackgroundColor<<abstract>> #F0E68C
    BackgroundColor<<enum>> #FFD700
}

skinparam stereotypeCBackgroundColor #7FFFD4

!define table(x) class x << (T,#FFDAB9) >>
!define primary_key(x) <b>x</b>
!define not_null(x) <u>x</u>
!define unique(x) <color:red>x</color>

' Facade Pattern 
class GameStateManager {
  - saveManager: SaveManager 
  - loadManager: LoadManager 
  - progressManager: ProgressManager 
  - settingsManager: SettingsManager 
  + GameStateManager(string)
  + SaveGame()
  + LoadGame()
  + UpdateProgress()
  + ChangeSettings()
}

class SaveManager {
  - programName: string 
  + SaveManager(string)
  + Save()
}

class LoadManager {
  - programName: string 
  + LoadManager(string)
  + Load()
}

class ProgressManager {
  - programName: string 
  + ProgressManager(string)
  + Update()
}

class SettingsManager {
  - programName: string 
  + SettingsManager(string)
  + Change()
}

GameStateManager "1" *-- "1" SaveManager : uses >
GameStateManager "1" *-- "1" LoadManager : uses >
GameStateManager "1" *-- "1" ProgressManager : uses >
GameStateManager "1" *-- "1" SettingsManager : uses >

@enduml
```
