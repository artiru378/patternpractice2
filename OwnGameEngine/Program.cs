﻿using System;

namespace DesignPatternsExample
{
    
    /// <summary>
    /// Интерфейс ICharacterBuilder определяет общие методы для создания персонажа, такие как SetAppearance, SetAbilities и SetEquipment. 
    /// Эти методы возвращают объект строителя, чтобы обеспечить возможность цепочки методов.
    /// </summary>
    public interface ICharacterBuilder<T> where T : ICharacterBuilder<T>
    {
        T SetAppearance(string appearance);
        T SetAbilities(string abilities);
        T SetEquipment(string equipment);
        void Build();
    }

    public abstract class CharacterBuilder<T> : ICharacterBuilder<T> where T : CharacterBuilder<T>
    {
        protected Character character;

        public T SetAppearance(string appearance)
        {
            character.Appearance = appearance;
            return (T)this;
        }

        public T SetAbilities(string abilities)
        {
            character.Abilities = abilities;
            return (T)this;
        }

        public T SetEquipment(string equipment)
        {
            character.Equipment = equipment;
            return (T)this;
        }

        //Метод Build создает объект персонажа и может вызывать другие методы по мере необходимости.
        public abstract void Build();
    }

    /// <summary>
    /// Классы ElfBuilder и DwarfBuilder предоставляют конкретные реализации интерфейса ICharacterBuilder для создания объектов Elf и Dwarf соответственно.
    /// Эти классы наследуются от абстрактного базового класса CharacterBuilder, который предоставляет реализацию общих методов построителя по умолчанию.
    /// Метод Build в этих классах устанавливает объект персонажа, задавая его свойства и вызывая другие методы по мере необходимости.
    /// Метод Get возвращает построенный объект персонажа.
    /// </summary>
    public class ElfBuilder : CharacterBuilder<ElfBuilder>
    {
        public ElfBuilder()
        {
            character = new Elf();
        }

        public override void Build()
        {
            // Call other methods here
            character.Race = "Elf";
            character.Level = 1;
        }

        public Elf Get()
        {
            return (Elf)character;
        }
    }

    public class DwarfBuilder : CharacterBuilder<DwarfBuilder>
    {
        public DwarfBuilder()
        {
            character = new Dwarf();
        }

        public override void Build()
        {
            // Call other methods here
            character.Race = "Dwarf";
            character.Level = 1;
        }

        public Dwarf Get()
        {
            return (Dwarf)character;
        }
    }

    /// <summary>
    /// Абстрактный базовый класс Character определяет общие свойства для всех персонажей.
    /// </summary>
    public abstract class Character
    {
        public string Appearance { get; set; }
        public string Abilities { get; set; }
        public string Equipment { get; set; }
        public string Race { get; set; }
        public int Level { get; set; }
    }

    /// <summary>
    /// Классы Elf и Dwarf представляют определенные типы персонажей и наследуются от базового класса Character.
    /// Эти классы имеют дополнительные свойства и поведение, специфичные для этих типов персонажей.
    /// </summary>
    public class Elf : Character
    {
        public int MagicPower { get; set; }

        public void CastSpell(string spell)
        {
            //Implement logic for casting a spell
            Console.WriteLine($"Elf casts {spell} spell with {MagicPower} magic power.");
        }
    }

    public class Dwarf : Character
    {
        public int MiningSkill { get; set; }

        public void MineOre(string oreType)
        {
            //Implement logic for mining ore
            Console.WriteLine($"Dwarf mines {oreType} ore with {MiningSkill} mining skill.");
        }
    }

    /// <summary>
    /// Класс GameStateManager действует как фасад, который обеспечивает единую точку доступа к основным подсистемам.
    /// </summary>    
    public class GameStateManager
    {
        private readonly SaveManager saveManager;
        private readonly LoadManager loadManager;
        private readonly ProgressManager progressManager;
        private readonly SettingsManager settingsManager;

        public GameStateManager(string programName)
        {
            saveManager = new SaveManager(programName);
            loadManager = new LoadManager(programName);
            progressManager = new ProgressManager(programName);
            settingsManager = new SettingsManager(programName);
        }

        public void SaveGame()
        {
            saveManager.Save();
        }

        public void LoadGame()
        {
            loadManager.Load();
        }

        public void UpdateProgress()
        {
            progressManager.Update();
        }

        public void ChangeSettings()
        {
            settingsManager.Change();
        }
    }

    public class SaveManager
    {
        private readonly string programName;

        public SaveManager(string programName)
        {
            this.programName = programName;
        }

        public void Save()
        {
            Console.WriteLine($"Saving {programName} game...");
            // Save game state
            Console.WriteLine($"{programName} game saved.");
        }
    }

    public class LoadManager
    {
        private readonly string programName;

        public LoadManager(string programName)
        {
            this.programName = programName;
        }

        public void Load()
        {
            Console.WriteLine($"Loading {programName} game...");
            // Load game state
            Console.WriteLine($"{programName} game loaded.");
        }
    }

    public class ProgressManager
    {
        private readonly string programName;

        public ProgressManager(string programName)
        {
            this.programName = programName;
        }

        public void Update()
        {
            Console.WriteLine($"Updating {programName} progress...");
            // Update player progress
            Console.WriteLine($"{programName} progress updated.");
        }
    }

    public class SettingsManager
    {
        private readonly string programName;

        public SettingsManager(string programName)
        {
            this.programName = programName;
        }

        public void Change()
        {
            Console.WriteLine($"Changing {programName} settings...");
            // Change game settings
            Console.WriteLine($"{programName} settings changed.");
        }
    }

    //Command pattern
    ///Для каждого действия, которое может выполнить персонаж, можно создать конкретные классы команд, которые реализуют интерфейс Command. 
    ///Каждый конкретный класс команд будет иметь ссылку на объект персонажа и вызывать соответствующий метод на этом объекте при вызове метода Execute.
    public interface ICommand
    {
        void Execute();
    }

    public class CastSpellCommand : ICommand
    {
        private Elf elf;
        private string spell;

        public CastSpellCommand(Elf elf, string spell)
        {
            this.elf = elf;
            this.spell = spell;
        }

        public void Execute()
        {
            elf.CastSpell(spell);
        }
    }

    public class MineOreCommand : ICommand
    {
        private Dwarf dwarf;
        private string oreType;

        public MineOreCommand(Dwarf dwarf, string oreType)
        {
            this.dwarf = dwarf;
            this.oreType = oreType;
        }

        public void Execute()
        {
            dwarf.MineOre(oreType);
        }
    }

    public class Invoker
    {
        private Queue<ICommand> commands;

        public Invoker()
        {
            commands = new Queue<ICommand>();
        }

        public void AddCommand(ICommand command)
        {
            commands.Enqueue(command);
        }

        public void ExecuteCommands()
        {
            while (commands.Count > 0)
            {
                ICommand command = commands.Dequeue();
                Console.WriteLine($"Executing command: {command.GetType().Name}");
                command.Execute();
            }
        }
    }

    public class Program
    {
        public static void Main(string[] args)
        {
            // Create a character using the Builder pattern
            var elfBuilder = new ElfBuilder();
            elfBuilder.SetAppearance("Tall and slender")
                      .SetAbilities("Archery and magic")
                      .SetEquipment("Bow and arrow")
                      .Build();
            Elf elf = elfBuilder.Get();
            elf.MagicPower = 10;

            var dwarfBuilder = new DwarfBuilder();
            dwarfBuilder.SetAppearance("Short and stocky")
                       .SetAbilities("Mining and smithing")
                       .SetEquipment("Pickaxe and hammer")
                       .Build();
            Dwarf dwarf = dwarfBuilder.Get();
            dwarf.MiningSkill = 5;

            Console.WriteLine($"Elf: {elf.Appearance}, {elf.Abilities}, {elf.Equipment}");
            elf.CastSpell("Fireball");
            Console.WriteLine($"Dwarf: {dwarf.Appearance}, {dwarf.Abilities}, {dwarf.Equipment}");
            dwarf.MineOre("Gold");

            Console.WriteLine();

            // Use the Facade pattern to manage game state
            GameStateManager gameStateManager = new GameStateManager("MyGame");
            gameStateManager.SaveGame();
            gameStateManager.LoadGame();
            gameStateManager.UpdateProgress();
            gameStateManager.ChangeSettings();

            Console.WriteLine();

            // Use the Command pattern to execute command
            Invoker invoker = new Invoker();

            ///пользователь может ввести такие команды, как cast spell fireball или mine ore gold, чтобы создать и вызвать объекты CastSpellCommand и MineOreCommand соответственно. 
            ///Когда пользователь вводит команду execute, объект Invoker выполняет все зачисленные команды по порядку. 
            ///Пользователь может выйти из программы, введя команду exit.
            Console.WriteLine("Enter commands (e.g., 'cast spell fireball', 'mine ore gold', 'execute'):");
            string input = Console.ReadLine();
            while (input != "exit")
            {
                string[] parts = input.Split(' ');
                string commandName = parts[0];
                string[] commandArgs = parts.Skip(1).ToArray();
                switch (commandName)
                {
                    case "cast":
                        if (commandArgs.Length > 1 && commandArgs[0] == "spell")
                        {
                            string spell = commandArgs[1];
                            invoker.AddCommand(new CastSpellCommand(elf, spell));
                        }
                        break;
                    case "mine":
                        if (commandArgs.Length > 1 && commandArgs[0] == "ore")
                        {
                            string oreType = commandArgs[1];
                            invoker.AddCommand(new MineOreCommand(dwarf, oreType));
                        }
                        break;
                    case "execute":
                        invoker.ExecuteCommands();
                        break;
                }
                input = Console.ReadLine();
            }
        }
    }
}
